[TOC]

**解决方案介绍**
===============
该解决方案可以帮助您快速搭建LNMP平台的WEB环境。主要用于国内大中型互联网公司搭建动态网站或程序。

解决方案实践页面地址： https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-lnmp-environment.html

**架构图**
---------------
![架构图](./document/rapid-deployment-of-lnmp-environment.png)

**架构描述**
---------------
该解决方案会部署如下资源：
  - 创建1台Linux弹性云服务器 ECS，安装Nginx、PHP以及MySQL，并完成相关配置，用个人网站的业务以及数据库节点。
  - 创建1个弹性公网IP，并绑定到弹性云服务器，用于对外提供网站的访问服务。
  - 创建安全组，可以保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口。

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-lnmp-environment
├── rapid-deployment-of-lnmp-environment.tf.json -- 资源编排模板
├── userdata
    ├── lnmp_install.sh  -- 脚本配置文件
```
**开始使用**
---------------
1. 登录[ECS弹性云服务器](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，选择创建的云服务器，单击远程登录，进入Linux弹性云服务器。

   **图1** ECS控制台

   ![ECS控制台](./document/readme-image-001.png)

2. 在Linux弹性云服务中输入账号和密码后回车。

   **图2** 登录ECS弹性云服务器

   ![登录ECS弹性云服务器](./document/readme-image-002.png)

3. 输入命令mysql -u root -p指定 root 用户登录 MySQL，输入后按回车键输入密码。使用 SET PASSWORD 修改密码命令格式为 set password for root @localhost= password('新密码');

   **图3** 修改MySQL的root 账号密码

   ![修改MySQL的root 账号密码](./document/readme-image-003.png)

4. 访问服务，打开浏览器，输入http://EIP/info.php，访问页面如下图所示

   **图4** 访问服务

   ![访问服务](./document/readme-image-004.png)
